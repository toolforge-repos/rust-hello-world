/*
This file is part of the Toolforge Rust tutorial

Copyright 2021-2023 Kunal Mehta and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#[macro_use]
extern crate rocket;

use anyhow::Result;
use mysql_async::prelude::*;
use mysql_async::Pool;
use rocket::http::Status;
use rocket::State;
use rocket_dyn_templates::Template;
use serde::Serialize;

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    /// The title variable
    title: String,
}

/// The context needed to render the "error.html" template
#[derive(Serialize)]
struct ErrorTemplate {
    /// The error message
    error: String,
}

/// Handle all GET requests for the "/" route. We return either a `Template`
/// instance, or a `Template` instance with a specific HTTP status code.
///
/// We ask Rocket to give us the managed State (see <https://rocket.rs/v0.5/guide/state/#managed-state>)
/// of the database connection pool we set up below.
#[get("/")]
async fn index(pool: &State<Pool>) -> Result<Template, (Status, Template)> {
    match get_latest_edit(pool).await {
        // Got the title, render the "index" template
        Ok(title) => Ok(Template::render("index", IndexTemplate { title })),
        // Some error occurred when trying to query MySQL, render the "error"
        // template with a HTTP 500 status code
        Err(err) => Err((
            Status::InternalServerError,
            Template::render(
                "error",
                ErrorTemplate {
                    error: err.to_string(),
                },
            ),
        )),
    }
}

/// Get the latest edit via the English Wikipedia ("enwiki") database replica
async fn get_latest_edit(pool: &Pool) -> Result<String> {
    // Open a connection and make a query
    let mut conn = pool.get_conn().await?;
    let resp: Option<String> = conn
        .exec_first(
            r#"
SELECT
  rc_title
FROM
  recentchanges
WHERE
  rc_namespace = ?
ORDER BY
  rc_timestamp DESC
LIMIT
  1"#,
            // You can avoid SQL injection by using parameters like this.
            // The "0" gets substituted for the "?" in the query.
            (0,),
        )
        .await?;
    // This unwrap is safe because we can assume that 1 row will always be returned
    Ok(resp.unwrap())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        // Create a new database connection pool. We intentionally do not use
        // Rocket's connection pooling facilities as Toolforge policy requires that
        // we do not hold connections open while not in use. The Toolforge crate builds
        // a connection string that configures connection pooling in a way that doesn't
        // leave unused idle connections hanging around.
        .manage(Pool::new(
            // Read from ~/replica.my.cnf and build a connection URL
            toolforge::connection_info!("enwiki_p", WEB)
                .expect("unable to find db config")
                .to_string()
                .as_str(),
        ))
        .mount("/", routes![index])
        .attach(Template::fairing())
}
